# Developer and End-User Migration Information
Communicating with the developers/end-users of your Gitlab projects is paramount for a smooth migration experience. The following points summarize the responsibilities and typical workflow for a customer’s developers or users who are responsible for the code, issues, merge requests, etc.. that are migrated.

## Before the migration

- Customer is to send out communications to their users notifying them of which groups or projects are to be migrated in the upcoming wave. This should be done at least once and at least 48 hours prior to the migration wave.
    

- Users should be aware that some or all of their Gitlab projects are in the process of being migrated to Gitlab.com
    
- Specific teams of users should know the tentative dates of when their specific projects are to be migrated
    

## Before a migration wave

- Members of user teams should perform the following tasks
    

- Check communications sent out by management to determine if any of their projects will be included in the next migration wave
    
- For projects included in the migration wave, make any last code changes, issues, merge requests, etc.. prior to the beginning of the migration wave
    
- Do not push any code changes or create any issues, merge requests, etc… until after management sends out a communication that the migration is complete
    

- Projects are only migrated once. If changes are made to the source self-managed instance during or after the migration, they will not be carried over to Gitlab.com
    

## During a migration wave

- Refrain from making any changes to any groups or projects involved in the migration wave
    
- Refrain from changing any settings at the user, group, or instance level
    

## After a migration wave

WARNING!!! DO NOT RUN ANY PIPELINES WITH THE EXPECTATION THAT THEY WILL COMPLETE SUCCESSFULLY. PIPELINES AND OTHER ELEMENTS OF GITLAB SUCH AS RUNNERS WILL MOST LIKELY NEED TO BE UPDATED BEFORE PIPELINES ARE SUCCESSFUL

  

- Members of users teams should update their git upstream origin on all of their local repositories so that for here on out they are pulling and pushing to/from GitLab.com
    
- Members of the application team should be aware that after a migration wave is complete, the projects involved in the migration wave will be archived on the source self-managed GitLab instance. This means that users will no longer be able to push code to those projects Any attempt to do so will result in a git error unless the project is unarchived which is not recommended
    
- The command to update your upstream origin is:
    

- git remote set-url <remote_name> <remote_url>
    
- Example git remote set-url origin [https://gitlab.com/customer group/project-path.git](https://gitlab.com/syngentagroup/project-path.git)
    

- Members of user teams should test that they can re-pull their projects from Gitab.com
    

- If they receive errors at this stage the most likely issues are:
    

- They need to generate a new ssh token
    
- They need to reauthenticate
    

- If a member of the user team is unable to push code the most common issues are:
    

- They are still pushing to the old instance where the project is archived
    
- They need to generate a new ssh token
    
- They need to reauthenticate
    

## Post-Migration Validation and Verification

- User teams should check on, among other things, the following elements of their projects on Gitlab.com:
    

- The number of branches on a given project
    
- The total number of merge requests
    
- Their role on the given project
    

## Updating and Running Pipelines

- In many cases pipelines contain specific hooks that point to various resources or other locations with the project structure
    
- If any hooks are pointing to a private instance or localhost for example, gitlab.com will see them as invalid and fail when creating them. Members of the user teams should update these hooks
    
- project and group CI/CD variables are migrated, but values that are source specific, e.g. project url or hostname, should be updated to the new values
    
- secrets (tokens) that may be present in certain features, e.g. hooks, are not exposed in the API response and therefore not migrated. Those individual features have to be newly created
    
- There may be other elements of your pipelines that need to be updated depending on your pipeline and underlying tech-stack but the previous examples are at least what you can expect to need to update