# Overveiw - Migration Plan
## Migration Engagement Workflow
1. Implement Security Configuration Plan
2. User import / Migration
3. Sample/Pilot Migration
4. Migration Wave Planing

### 1. Implenting Security Configuration Plan
* The majority of the security configuration is handled by the SSO configuration as only SAML authenticated users can access the customers top-level group on gitlab.com
* Other security options are available including:
* Ip allowlisting: Settings > General > Restrict access by IP address
* Restricted access based on email domain: Settings > General > Restrict membership by email domain
* Assuming a migration to Gitlab.com, if SSO is implemented for the customer top-level group then in the settings > SAML SSO you can enforce SAML only authentication which will prevent any non-SAML user from gaining access to the group. The setting may not be enabled during the migration to ensure the PS engineer has proper access to perform the migration tasks.
* 2FA can be enforced in the group, but may be redundant if SSO is already enforced as that usually requires a Multi-Factor element
### 2. User Import / Migration
#### SSO Configuration
* The first active step in a migration is making sure that all of a customers active users exist on Gitlab.com, this is typically done via a mass import from an SSO application that the customer manages
* Users need to exist on Gitlab.com prior to migration so issues, merge requests, comments, etc.. are attributed to the correct user based on the email address associated with their account
[Gitlab.com SSO documentation](https://docs.gitlab.com/ee/user/group/saml_sso/)
[Git.com SCIM documentation](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html)
#### User Creation
* If SSO is not being utilized during the migration, then our Congregate Migration Tool can create users as part of the migration process. All users must be created before any groups are projects are migrated to ensure proper mapping between the source and destination systems
### 3. Sample/Pilot Migration
* After the customer has successfully imported their users onto Gitlab.com we can perform a sample migration, also known as a pilot migration or a migration POC
* The purpose of a sample migration is to move a small set of customer projects onto Gitalb.com so that PSE can troubleshoot any potential issues prior to a full migration
* A sample migration gives the customer a look into what their projects and groups will look like on Gitlab.com
* The customer will also be able to see what elements of their projects and groups they may have to update and adjust with regards to migrating from a * Self-managed instance to Gitlab.com
* During this time the PSE can assist the customer if they have any questions or concerns prior to the full migration
### 4. Migration Wave Planning
#### Scheduling and constraints 
- Once the sample migration is complete and has been validated by the customer the migration workflow can begin
- Migration waves are comprised of up to 400 projects and the total time for a migration can take 6-12 hours to complete based on the size and number of the projects involved in a particular wave
- If migrating to gitlab.com, a migration wave is scheduled with a customer at least 5 days in advance in order to give the PSE time to ensure that no other customer migrations to GitLab.com will take place during the migration window
- After a wave is complete, The Professional Services Engineer will archive the projects that were migrated on the source instance. This is to force users to adhere to a single source of truth and to continue any development on the GitLab destination.
- Migration waves will commence until all waves are completed
