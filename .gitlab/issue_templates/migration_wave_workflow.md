# Migrtaion Wave Workflow
## Scheduling and constraints 
- Once the sample migration is complete and has been validated by the customer the migration workflow can begin
- Migration waves are comprised of about 400 projects and the total time for a migration takes about 8 hours to complete based on the size of the projects involved in the particular wave
- When migrating to GitLab.com, a migration wave is scheduled with a customer at least 5 days in advance in order to give the PSE time to ensure that no other customer migrations to GitLab.com will take place during the migration window
## User communication

## Workflow
1. At least a few hours before the migration is scheduled to begin, activities involving pushing code changes, creating Issues, and creating Merge requests should be suspended
    - Any changes made to the customer system during the migration may lead to code changes and other features not being migrated to Gitlab.com

2. At the time of the migration the PSE will contact the customer asking for final approval to commence the migration

3. After customer approval, the PSE will perform a dry-run to ensure the migration will proceed as expected. The PSE will alert the customer when the dry-run is complete

4. After a successful dry-run, the PSE will commence the migration and monitor the status until it is complete giving the customer status updates throughout the process

5. After the Migration is complete and the PSE observes no issues, they will alert the customer that the migration is complete. If issues are detected certain migration steps may be repeated

6. After the migration wave is complete with no issues detected by the PSE, the standard process is for the PSE to Archive the projects on the customer’s gitlab instance to prevent users from pushing updates to the old system

7. The next step in the process is customer validation. During this time the customer should instruct users/teams involved in the projects that were migrated check the projects to ensure that everything looks correct
    - The customer can also take this opportunity to have users adjust the elements of their projects that were affected by the migration (for details see the section of this document titled - What typically needs to be "fixed" in a migrated project? )
    - If the customer finds critical errors such as missing branches or other data, the PSE will delete the affected projects off of Gitlab.com and remigrate some or all of the projects.

8. Once customer validation is complete, preparations for the next wave begins. 

## Customer Responsibilities During a Migration Wave
- The customer is responsible for communicating with their users to ensure that they do not make any changes to their source gitlab instance during a migration wave
    
- The customer is responsible for defining the projects that will be in each migration wave
    
- After a migration wave has been completed by the PSE, the Customer is responsible for validation/verification of the projects moved