# Security Configuration Plan
## SSO 
The customer’s SSO configuration plan is what constitutes the majority of their security configuration. With SSO enabled and enforced, the customer has total control over user provisioning and management. Only users imported via the SSO application have any level of access to the customer group on GitLab.com.

## Recommended Top-Level Group Settings
These settings can be enabled by going to Gitlab.com/\<your namespace> and going to **Settings > General > Permissions and group features**  
|**Members cannot invite groups outside of \<your namespace> and its subgroups**|
|-|
This is available only on the top-level group. Applies to all subgroups. Groups already shared with a group outside Hershey are still shared unless removed manually. 
 
|**Projects in \<your namespace> cannot be shared with other groups**|
|-|
Applied to all subgroups unless overridden by a group owner. Groups already added to the project lose access

|**Prevent forking outside of the group**|
|-|
This prevents any external forking of your organization's project   

## Additional / Supplementary Security Settings
If a customer is not implementing SSO or is allowing regular Gitlab.com accounts to access any of the organization's groups or projects, then there are some additional security settings that can be implemented. These settings can be enabled by going to Gitlab.com/<your namespace> and going to **Settings > General > Permissions and group features**
* **Restrict access by IP address**
  * The customer can restrict access to their group to a specific series of ip address ranges. This is useful if the customer desires to force users to access gitlab over a VPN or other remote network resources
* Restrict membership by email domain
  * The customer can restrict access to their group to users whose email address belongs to a specific domain. This setting is typically redundant when the customer enforces SSO

* **Two-factor authentication**
  * You can force all members of your organization to setup 2FA on Gitlab.com in order to access Groups and Projects. This setting is typically redundant when the customer enforces SSO as SSO applications already enforce 2FA