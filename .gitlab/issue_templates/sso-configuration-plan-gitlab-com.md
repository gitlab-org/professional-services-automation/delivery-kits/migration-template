# SSO Configuration Plan - GitLab.com 
## Customer Requirements:
* [ ] Customer has reserved a group namespace prior to engagement
* [ ] Customer license level is GitLab Premium SaaS or higher
* [ ] Customer will support SSO configuration with IdP Administration.

## Role Changes on GitLab.com
* On Gitlab.com the admin role will not be available. The highest role a user can have is Owner.
* This means users will not have access to instance level controlls or metrics
* Namespace owners will not have admin-level control over thier users and will instead need to leverage their SSO tenent to manage users at a high-level
* Unlike a self-managed instance, all users will need to be a member of your Organization at the top-level. In virtually all cases you will want their default role to either be guest or minimal access
* If your organization has an Ultimate liscense, any uses that are purely Guest or Minimal access will not take up a subscription seat.
* As soon as a user is given the reporter role or higher on any sub-group or project, they will then take up a seat
* Subgroup and Proejct specific roles will migrate over from your source system.

## Leveraging your SSO Tenent
* Namespace owners cannot provision and deprovision users via gitlab.com
* The provisioning and deprovisioning of users can be managed via your SSO tenet
* Setting up domain verication along with SCIM (See Links below) will allow you to provision and claim users for your organization automatically and restrict what they are aloud to do outside of your oganization with their Gitlab.com account.

## Specifications
| Customer Namespace | TODO |
|-|-|
| **IdP Application** | **TODO** |

## GitLab Settings
Under Gitlab.com/<your namespace> **Settings > SAML SSO** Note the following settings:
* **Enable SAML authentication for this group**
  * Check this box and leave it checked permanently to allow SAML authentication
* **Enforce SSO-only authentication for web activity for this group**
  * leave this box unchecked **UNTIL THE END OF THE MIGRATION** after migration activities have concluded, check this box to force all users to authenticate via SAML in order to access your Gitlab.com organization

## SSO Elements 
#### **SAML Application**  
Allows users to login via a tile on your Organizations SSO application  
[How to Configure](https://docs.gitlab.com/ee/user/group/saml_sso/)  
[Additional SAML assertions](https://docs.gitlab.com/ee/user/group/saml_sso/index.html#supported-user-attributes) - Requires setting up Domain Verification  
Access your SAML settings on GitLab.com/<your namespace> under **Settings > SAML SSO**  

#### **SCIM Application**  
Allows your organization to auto-provision and de-provision users.  
[How to Configure](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html)  
**NOTE if your organization chooses not to setup SCIM, then prior to any Group or Project migration, every user must login via their SAML tile once in order for their user to be created**  
**NOTE the SCIM application will be separate from your SAML application, you should keep this tile hidden from your users.**  
Access your SCIM Settings on GitLab.com/<your namespace> under **Settings > SAML SSO**  

#### **Domain Verification**  
Auto-Claims your organizations users. This tags them as **Enterprise users**  
This prevents users from having to respond to a GitLab automated email when logging in for the first time. This makes the user experience much smoother  
[How to Configure](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/)  
This allows you to add [additional SAML assertions](https://docs.gitlab.com/ee/user/group/saml_sso/index.html#supported-user-attributes) to your users:     * Prevent Users from being able to create personal projects **Recommended to restrict Organizational IP**  
* Prevent Users from being able to create top-level groups on GitLab.com **Recommended to restrict Organizational IP**