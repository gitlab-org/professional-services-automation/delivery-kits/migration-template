## Migration Wave Execution

These are the steps you will follow for any migration wave.  It is broken up into two steps, wave setup and wave execution.  Wave setup commands can be run up to 24 h before the migration time frame window.  Wave execution commands should be followed during the Migration time frame window.


### Wave Setup (Up to 24 h lead time)

- Connect to the migration VM/Container. These are long running processes, so it is suggested to use Screen or TMUX.
- Determine if your Congregate list data is up to date. Git changes do not matter, but changes to group structure or membership can. If you need to update the json files, from the migration VM; run `congregate list` to update the local data.
- Backup the congregate.conf to the appropriate file if required, i.e. `cp data/congregate.conf data/congregate.conf.gitlab`.
- Copy the correct congregate.conf.template for your source type, i.e. `cp data/congregate.conf.gitlab data/congregate.conf`.
- Stage the data to be migrated. This is a non-destructive command, so no dry-run is necessary, so we supply the –commit.
    - If projects; `congregate stage-projects {123456} {987654} –commit`
    - If groups; `congregate stage-groups {123456} {987654} –commit`

### Wave Execution (Migration Time Frame Window)

- **DRY-RUN** Migrate the staged Groups; `congregate migrate –skip-users –skip-project-import –skip-project-export`.    
    - Suggested to do this before the actual migration window to save time.
- **MIGRATE** the staged Groups.
    - If the dry-run was successful (no errors, no complaints about users missing public emails), and the number of groups make sense, rerun the previous command with a commit; `congregate migrate –skip-users –skip-project-import –skip-project-export –commit`.
- Make sure the totals in the output of the congregate run match expectations.  If we were migrating 100 groups, make sure all 100 were successful.  If not, run again.
- Communicate the start of the migration wave.
- Connect to the migration VM/Container. These are long running processes, so it is suggested to use Screen or TMUX from the Docker Host.
- Verify the correct congregate.conf is being used for your designated source.
- **DRY-RUN** migrate the staged Projects; `congregate migrate –skip-users –skip-group-import –skip-group-export`.
- **MIGRATE** the staged Projects.
    - If the dry-run was successful (no errors, no complaints about users missing public emails), and the number of projects make sense, rerun the previous command adding a commit; `congregate migrate –skip-users –skip-group-import –skip-group-export –commit`. 
        - This command will take the most time, and be most prone to errors. If the command completes successfully, but reports a success number lower than total number, keep rerunning the command until all successful, or no new successes.
- Handle Failures. 
    - If Successes do not match Total, try alternate methods of import; Curl and API with either S3 file based import, or Direct Transfer.
- If you had to manually handle exceptions, run the congregate migration again to sync up the results file for diff report; `congregate migrate –skip-users –skip-group-import –skip-group-export –commit`.
- Run **DIFF-REPORT**; `congregate generate-diff --staged --skip-groups`. 
    - Remember when reviewing the HTML file, that any individual project over 90% is considered successful.
- *OPTIONAL*: to prevent accidental writes to old repositories, consider archiving staged projects; `congregate archive-staged-projects –commit`. 
    - If your migration successes do not match the total, you might have to go manually unarchive the failures on the source instance.
- Clean up the migration VM by running “wave_archive.sh”. You will be prompted to input a name for the resulting tar file. This will produce a name.tar.gz file of all the related logs and result files for this run in /opt/congregate/data/archive
- Communicate the end of the migration wave. Think about including Success and Failure Totals and the Diff Report.

## Glossary

This document uses the following terms:

- **Congregate** - An [orchestration tool](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate) that wraps the GitLab built in import/export API’s. Built and maintained by GitLab Professional Services.
- **Evaluate** - An [information gathering tool](https://gitlab.com/gitlab-org/professional-services-automation/tools/utilities/evaluate) that helps teams prepare for migrating a GitLab instance.
- **GET** - The [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/gitlab-environment-toolkit), which contains template Terraform plans and Ansible playbooks, for deploying the Reference Architectures to setup new GitLab instances.
- **GPT** - The [GitLab Performance Tool](https://about.gitlab.com/handbook/support/workflows/gpt_quick_start.html), a performance testing tool for a GitLab instance.  Useful for evaluating performance after upgrades.
- **Wave** - A logical organization of projects to be migrated in a given time frame.
- **Source** - The instance to migrate projects, groups, and users FROM. The source instance types are commonly GitLab, Bitbucket, and GitHub.
- **Destination** - The GitLab instance to migrate projects, groups, and users TO.
- **Stage** - A Congregate command that prepares Congregate to migrate a given subset of source data.
- **List** - A Congregate command that gathers all of the source data.
- **dry-run** - A default Congregate method that simulates a given command without making any changes to source or destination. Dry-run should be the default for any command, supply a –commit argument to override this behavior.
- **Transfer Methods** - The various methods of importing and exporting to GitLab.
    - File Based - The default method for GitLab data. Has time and size limitations.
    - Direct Transfer - A new GitLab method that utilizes streaming, allowing a wider range of projects to be imported.
- **Reference Architecture** - designed and tested by the GitLab Quality and Support teams to provide recommended deployments at scale for [up to 3,000 users](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html).
- **Migration VM** - Usually a docker container running congregate that has https access to both source and destination instances.

## Links

A small collection of links with descriptions that are often useful for various GitLab related Activities.

### Congregate Specific Links

- [migrations-to-dot-com.md](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/runbooks/migrations-to-dot-com.md?ref_type=heads) - Generic Congregate runbook for migrating to .com.
- [pre-and-post-migration.md](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/customer/pre-and-post-migration.md) - General Congregate pre and post work. Fairly long and verbose.
- [gitlab-migration-features-matrix.md](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/customer/gitlab-migration-features-matrix.md?ref_type=heads) - Congregate list of what will be migrated from GitLab. Without a destination admin token, it will default to the gitlab export/import column.
- [bitbucket-migration-features-matrix.md](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/customer/bitbucket-migration-features-matrix.md?ref_type=heads) - Congregate list of what will be migrated from Bitbucket Server. Without a destination admin token, it will default to the GitLab BBS importer column.
- [github-migration-features-matrix.md](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/customer/github-migration-features-matrix.md?ref_type=heads) - Congregate list of what will be migrated from GitHub. Without a destination admin token, it will default to the GitLab GH importer column.
- [congregate.conf.template](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/congregate.conf.template) - Default congregate.conf file with suggested fields for different options such as max retries, source configuration, threading, etc.

### General Links

- [Runner Vending Machine](https://gitlab.com/guided-explorations/aws/gitlab-runner-autoscaling-aws-asg/) - A quick way to spin up basic runners in AWS.
- [Direct Transfer](https://docs.gitlab.com/ee/api/bulk_imports.html) - Docs for migrating groups and projects with Direct Transfer, via the API. Currently this feature is considered beta for groups with projects, but is the recommended way to migrate groups specifically. For now, this feature should be considered for GitLab projects that fail all other methods.
- [Group Recommendations](https://about.gitlab.com/blog/2023/05/31/securing-your-code-on-gitlab/) – GitLab blog post about securing your GitLab environment with best practices.
- [Project Limitations](https://gitlab.com/gitlab-org/professional-services-automation/tools/utilities/evaluate#project-thresholds) - Evaluates project limitations.  Projects that exceed these values have a higher chance of failing automatic migrations.
