#!/bin/sh

if [ "$CI_PROJECT_URL" == "https://gitlab.com/gitlab-com/customer-success/professional-services-group/project-templates/migration-template" ]; then
    git config user.name "$GITLAB_USER_EMAIL"
    git config user.email "$GITLAB_USER_NAME"
    git remote set-url origin https://$GITLAB_USER_LOGIN:$ACCESS_TOKEN@gitlab.com/gitlab-com/customer-success/professional-services-group/project-templates/migration-template.git
    git reset --soft $(git rev-list --max-parents=0 HEAD)
    git commit --amend -m "Initial Commit [skip ci]"
    git push origin HEAD:$CI_COMMIT_REF_NAME -f
else
    echo "Assuming this is a project using the migration-template project template. Skipping rebase"
fi

