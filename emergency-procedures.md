# Emergency Procedures for GitLab.com Migrations

Note: The processes in this document are for "world on fire" level emergencies. A day or two slip in a migration schedule *DOES NOT QUALIFY*. Some of the steps listed here are morally ambiguous from a security perspective and should not be used at any other time.

## Assumptions

* Migration VM exists and was created using latest templates from 2023-10-19
* Admin account for GitLab.com is required
* You have a SSH key to add to the VM to allow someone other than the primary owner to login (public key)
* You know the IP address of the new user being added to a VM
* Specialized configuration (client VPN, client laptop, etc) is not required
* Someone is available that has console access to the GCP space for the migration VM

## Steps

1. Request a new admin token via the standard AR process
  - Note: *DO NOT SHARE EXISTING ADMIN CREDENTIALS*
  - Note: This can take 24 hours, so this process is not fast
  - Note: In the case of Direct Transfer, this is only a group owner token on the destination instance
1. Access the [VM Dashboard](https://console.cloud.google.com/compute/instances?project=transient-imports) and find the appropriate machine in the list
1. On the machines line in the list, click the `nic0` link for the machine
1. Scroll down to `Firewall and routes details` and expand `vpc-firewall-rules`
1. Locate the rule ending in `fw-internal-deny` and click on it
1. `Edit` the rule (button at top of page), scoll down to `Disable rule`, expand it, and select `Disabled`
1. Save
1. Now comes some seriously shifty part
1. Under the `vpc-firewall-rules` there should be a rule named `ps-congregate-` and some number or customer identifier. Eg: `ps-congregate-17166` or `ps-congregate-mycustomer`. Click on that and then `Edit`
1. Under `Source IPv4 ranges` enter `0.0.0.0/0` and `Save`
  - Note: *YOU HAVE JUST EXPOSED THIS MACHINE TO EVERYTHING*
1. Back on the [VM Dashboard](https://console.cloud.google.com/compute/instances?project=transient-imports), locate the `SSH` button on the far right of the line for the VM in the list
1. Click the dropdown and select `Open in browser window`
1. A console window should open up in the browser. From here, you can follow standard instructions for [adding a new SSH key to an Ubuntu box](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04) to add a key for the additional user
1. Go back and `remove` the `0.0.0.0/0` from above but *ADD* the external IP for the new user
1. Go back and `re-enable` the `fw-internal-deny` rule
1. Test SSH access for the new user using their key from the IP that they provided

## Alternate Safer Steps for the VM Portion

To limit the exposure vector to the VM, a GCP user with the proper permissions (project owner or editor) can use the `gcloud cli` to access the box. You will still need:

* Someone with elevated permissions (hereby called `elevated user`) on the transient-imports `GCP` project
* Someone with maintainer permissions (hereby called `maintainer`) on the transient-imports `GitLab` project
* An SSH keypair for the new user to add to the VM
* The IP addresses of the new user *and* the elevated user
* The elevated user will need the gcloud cli installed and configured to connect to the `transient-imports` GCP project

1. new user : File an MR in the [transient-imports project](https://gitlab.com/gitlab-com/gl-infra/transient-imports) to add the your IP *and* the IP of the user with elevated permissions that is able to perform the work
1. maintainer : Merge the MR and run the pipelines to apply it to the VM
1. new user : Send the SSH public key to the elevated user
1. elevated user : Connect to the VM using `gcloud compute ssh`. This should work once your IP has been applied to the firewall.
1. elevated user : `sudo su` and add the new user's public key to the `~/.ssh/authorized_users` file
1. new user : test ssh to the box using your matching private key
