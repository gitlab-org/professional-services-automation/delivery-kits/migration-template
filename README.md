# Migration Delivery Kit

:rotating_light: :rotating_light: :rotating_light:

**Please Note:** This project has been archived, and its content has been moved over to the [migration-template folder within the Migration Delivery Kit](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/migration-delivery-kit/-/tree/main/Congregate/Migration-template?ref_type=heads).

*This project aims to gather together the steps to deliver migration services for a customer.*

## Project Initiation

1. As the delivering PSE, you should have access to the signed SOW, the scoping issue (with straw-man link), and the customer internal slack channel. **If this is not the case, ask in [#ps-internal](https://gitlab.slack.com/archives/CFY1QN4FJ) for assistance.**
1. Review the SOW and scoping issue for details about what was scoped. Ask clarifying questions to the Engagement Manager who scoped it like _"Am I hands on keyboard for this part?"_,  _"Do you have an example of XX deliverable (I've never done that before) "_, or _"I think this is underscoped based on the data in the straw man, can you help me understand the rationale?"_.
1. Once you have the context of what was scoped (including rationale and customer goals), make a copy of the [project Kickoff deck](https://docs.google.com/presentation/d/1F3u7rSQthgukHQiMFML9ZrKXCgyYoQZ_6TZmOKLPYpY/edit?usp=sharing) and update with the details for this specific engagement. Put it into the project folder and the PM will update it with thier information.
1. Schedule the kickoff call. Be sure not to go into the weeds of the engagement. This is to get agreement on schedule, scope, communications, access requests and to set expectations

## Typical Migration Activities

1. **Migration Assessment**:
   - Prior to a migration, a customer may request an assessment of the migration options. [This](https://docs.google.com/document/d/1qDJ3ONzSEilrmnHJrzeMD-0Cv_S2EXXFmGa3MAJ1Xy8/edit?usp=sharing) (GitLab internal) template can be used to fill that need. Please update the template as information changes.
1. **Discovery and planning with customer**:
    - Prep: **prior to customer kickoff meeting**:
       - Create a project under the  appropriate customer collaboration group using this template. You can do so by creating it from template (*New project -> Create from template -> Group (tab) -> PS Migration Template Mirror*).

      ![PS Migration Template Mirror](img/migration_template.png)

       - If you cannot find the customer collaboration group, check with the Professional Services Project Manager.  A few things to note about customer collaboration projects:
       - Create an issue in the customer collaboration project using the [pre- and post- setup issue template](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template/issues/new?issuable_template=migration-pre-and-post-requisites) and walk through the details to ensure you know what questions to ask during discovery meeting(s). 
       - Make a copy of the [migration kickoff deck](https://docs.google.com/presentation/d/1AzM_qYKKOYhgvNTrEBXRmFT2m0caBuKZ6VAH6sCbiKQ/edit#slide=id.g911d82cfdc_1_5) and modify with the details from the SOW.
       - Schedule and hold the kickoff meeting with the customer to get agreement on migration details. Note: this meeting is typically scheduled by the project manager
    - During the **kickoff meeting** with the customer - show the customer the `customer` folder in the collaboration project so they know where to find documents that will help them prepare for the migration
    - During the **discovery meeting** with the customer
       - Go through a discovery session with the customer to understand their specific usage of GitLab.  The goal is to identify any impacts of migration that they might not have considered.  Ask about:
          - (GitLab -> GitLab) Check [version requirements](https://docs.gitlab.com/ee/user/project/settings/import_export.html#version-history) for the source and destination systems. In general, the source system can only be two (2) minor versions behind the destination system once the migrations start.
          - Number of users and projects involved in each wave
          - Runners in use
          - Group/Project structure for the wave
          - Cross-project dependencies
          - Size of registry
          - Integrations and webhooks in use
          - Communications with internal and external systems
          - Other areas with large amounts of data - MR's, pipelines, etc.
          - Use of secrets / deploy tokens / deploy keys
       - Consult the relevant migration feature matrix to showcase which features are in scope for the migration
          - [GitLab](customer/gitlab-migration-features-matrix.md)
          - [BitBucket](customer/bitbucket-migration-features-matrix.md)
          - [GitHub](customer/github-migration-features-matrix.md)
       - Discuss specific requirements
          - Mapping users public email on source systems in GitLab to GitLab scenarios
          - Account ownership vs administrative rights on self-managed to GitLab.com scenarios
       - Discuss change management and communications to prepare end users for the migration: [customer migration change management](customer/customer-migration-change-management.md)
       - Discuss [migration wave considerations](customer/migration-wave-considerations.md)
       - Discuss [group, project, and user conflict resolution](customer/group-project-and-user-conflict-resolution.md) and document customer agreed upon resolution strategy
       - For any open questions consult the [Frequently Asked Migration Questions](customer/famq.md)
       - Review [Definition of Done](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template/-/blob/master/customer/definition-of-done.md) and get agreement from customer stakeholders, or modify DoD and acceptance criteria to meet their needs.  Any customer-specific changes to the DoD should be made to the source file that is in the `customer` folder of the migration customer collaboration project, with a note that it is a change from the standard template

1. **Congregate Preparation - Asynchronous**: This is time for the PSE to configure the congregate tool for the customer's migration.  This activity is done asynchronously, i.e. there is no need for the customer to be part of the activity. The [pre- and post- setup issue template](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template/issues/new?issuable_template=migration-pre-and-post-requisites) provides a checklist of steps for setting up congregate.  If you need help configuring congregate, you can post in the `congregate_internal` Slack channel, and/or join the congregate working session at 11am ET daily.
1. **User Migration and Inspection with Customer**: If the customer is using SCIM or the users already exist, no user migrations will need to be done.  This is the most common scenario for SAAS.  If the customer needs to migrate users, it is suggested to migrate them before the project and group migration. This is outlined in the migration runbook issue that you can create from the issue template in this project. For this and the following project and group migrations, use the [source specific migration runbook templates](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template/issues/new?issuable_template=migration-pre-and-post-requisites). Create an issue for each wave
1. **Sample Group and Project Migration and Inspection with Customer**: Use the issue template in this project to select the appropriate runbook for the source + destination system combination. Running through this with a small number of projects with the customer will enable you to validate the **functionality** of the migration. Have the customer inspect the migration results and point out anything that doesn't look right.
1. **Congregate Adjustment - Asynchronous**: Based on the previous activity, there may be some adjustments (config or code changes) that need to be made and tested prior to running the full migration.
1. **Migration Orchestration and Spot Checks**:
    - Once you have the migration schedule, create issues documenting every wave scheduled and at-mention the necessary people/teams for their awareness per requirements listed in the respective runbooks.
    - During migration execution, start a slack thread (if using slack) with operational migration details. Can add updates to that, but capture the migration details and results in the issue.

# Migration Approach

Based on the engagement and Customer specs, how do you decide which migration approach to use when we have multiple options such as Direct Transfer, File-based import/export, GEO migration, or the Backup & Restore route? 

[Here are some general guidelines](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/customer/migration-methods.md?ref_type=heads). 

We also have a [template document with the above information](https://docs.google.com/document/d/1xDkTbn4cMygQuAY-KinS0VhfKFpeM977kLHRPIe0cmI/template/preview). 

# Migration Project Template (for GitLab Internal Use)

To ease the delivery of the migration service, there is a standard migration plan document that can be presented to the customer. Click on the `USE TEMPLATE` button on the top right of the [PS Migration Plan](https://docs.google.com/document/d/1w3srV4CZQbNMqqMymH0l1CJhY2g33-p2rkmNm8Be3Bk/template/preview) to generate your own copy of the document. The document is pre-loaded with certain variables so ensure you change the following variables and that change will automatically populate through the rest of the document: 

- Customer_Name
- Source_System
- Destination_System

Please contribute sections that you had to elaborate on for the customer back to the template by creating an issue in this project or reaching out to the #ps-practice team.

**Note:** This PS Migration Plan template document lives within the template gallery in the [GitLab Google Drive](https://docs.google.com/document/u/0/?ftv=1&tgif=d), under *Professional Services*. This is where all of our standard delivery documents will be housed going forward. Any partners can request a GitLab team member to provide them a copy of the document if required. 

## About this Project

The runbooks are generated from the [congregate codebase](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate).

_Do not change the issue templates in this project. They are maintained in the congregate codebase and must be changed there. If you change the issue templates in this project template, they will be overwritten by an automated process from the congregate codebase._

_Edit this delivery kit by following our [contribution guidelines](/CONTRIBUTING.md)._
